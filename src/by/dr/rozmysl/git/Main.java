package by.dr.rozmysl.git;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Hello. What is your name?");
        String name = in.nextLine();
        System.out.println(name + ", nice to meet you!");
        System.out.println(name + " how are you!");
        System.out.println("I hope you are OK");
    }
}
